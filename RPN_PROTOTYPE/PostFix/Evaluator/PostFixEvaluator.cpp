#include "PostFixEvaluator.h"
#include "../../Common/Operator/Properties/OperatorProperties.h"
#include "../../Common/Utils/Utils.h"
#include "../Parser/PostFixParser.h"
#include "../Parser/IPostFixParser.h"

using std::string;
using std::vector;
using std::endl;

PostFixEvaluator::PostFixEvaluator(std::string input, IPostFixParser* parser, iStack<string>* stack) :
    IPostFixEvaluator(input, parser, stack) {}



PostFixEvaluator::~PostFixEvaluator()
{}

long double PostFixEvaluator::Evaluate()
{
    logger.str(std::string());

    string postFixStatement = parser->ConvertToPostFix(inFixInput);
    vector<string> tokens = Utils::split(postFixStatement, " ");
    RpnOperator::OperatorFactory operatorFactory;

    for (auto token : tokens)
    {
        if (token.compare("") == 0)
            continue;

        if (Utils::isOperator(token) == true)
        {
            logger << "Found operator: " << token << " Performing Evaluation" << endl;
            auto operatorTokenPtr = operatorFactory.getOperator(token);
            operatorHandler(operatorTokenPtr);
            delete operatorTokenPtr;
        }
        else
        {
            logger << "Token: " << token << " Pushed to stack" << endl;
            stack->push(token);
        }
    }
   

    auto EvaluationResult = std::stold(stack->tryPop());
    logger << "Final result pop-ed from stack: " << std::to_string(EvaluationResult) << endl << endl;
   
    return EvaluationResult;
}



void PostFixEvaluator::operatorHandler(Operator* operatorToken)
{

   
    auto operand2 = std::stold(stack->tryPop());
    auto operand1 = std::stold(stack->tryPop());
   
    auto result = std::to_string(operatorToken->Evaluate(operand1, operand2));
    stack->push(result);

    logger << endl <<"Pop-ed form stack: " << endl;
    logger << "Operand_1: " << operand1 << endl;
    logger << "Operand_2: " << operand2 << endl;
    logger << "Operation symbol: " << operatorToken->getSymbol() << endl;
    logger << "Operation result:  " << result << endl;
    logger << "Pushing result to stack" << endl << endl;
}


std::string PostFixEvaluator::getLogs()
{
    return logger.str();
}
