#pragma once
#include "../Parser/IPostFixParser.h"
#include "../../Common/Stack/iStack.h"
#include <string>

class IPostFixEvaluator
{
public:
	virtual long double Evaluate() = 0;
	
	virtual void addInFixInput(std::string input) { this->inFixInput = input; }
	virtual void attachParser(IPostFixParser* another) { if (parser != nullptr) delete parser; this->parser = another; }
	virtual void attachStack(iStack<std::string>* another) { if (stack != nullptr) delete stack; this->stack = another; }	

protected:
	IPostFixEvaluator() : inFixInput(""), parser(nullptr),stack(nullptr) {}
	IPostFixEvaluator(std::string input, IPostFixParser* parser, iStack<std::string>* stack) : inFixInput(input), parser(parser), stack(stack) {}

	std::string inFixInput;
	IPostFixParser* parser;
	iStack<std::string>* stack;
	virtual ~IPostFixEvaluator() { delete parser; delete stack; }

};