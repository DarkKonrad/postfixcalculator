#pragma once
#include "IPostFixEvaluator.h"
#include "../Parser/IPostFixParser.h"
#include "../Parser/PostFixParser.h"
#include "../../Common/Operator/OperatorFactory.h"
#include "../../Common/Operator/Base/OperatorBase.h"
#include "../../Common/Stack/NativeStack.h"

#include <sstream>
typedef RpnOperator::OperatorBase Operator;

class PostFixEvaluator : public IPostFixEvaluator
{
public:
	virtual long double Evaluate() override;
	PostFixEvaluator(std::string input = "", IPostFixParser* parser = new PostFixParser(), iStack<std::string>* stack = new NativeStack<std::string>());
	std::string getLogs();
	virtual ~PostFixEvaluator();

protected:
	std::stringstream logger;
	virtual void operatorHandler(Operator* token);
};

