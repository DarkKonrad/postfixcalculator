#include "PostFixParser.h"
#include "../../Common/Operator/Base/OperatorBase.h"
#include "../../Common/Stack/NativeStack.h"

#include <iostream>
#include <stack>
#include <exception>

using std::string;
using std::vector;
using std::stack;
using Utils::isOperator;




string PostFixParser::ConvertToPostFix(std::string statement)
{
    
    string output="";
    // output.append(" ");
    unsigned int inc = 0;
    for (auto character : statement)
    {
       
        if (!isOperator(character) && character != ')')
        {
            output.push_back(character);
        }
        else if (character == '(')
        {
            string buff = "";
            buff.push_back(character);
            opstack->push(buff);         
        }

        else if (character == ')')
        {
            auto topToken = opstack->tryPop();
           
            while (topToken != "(")
            {
                output.append(" ");
                output.append(topToken);
                topToken = opstack->tryPop();
            }
        }
        
        else if (isOperator(character))
        {
            inc++;
            auto token = RpnOperator::OperatorProperties::FindOperatorProperties(character);
            
            while (!opstack->isEmpty()&& (RpnOperator::OperatorProperties::FindOperatorProperties(opstack->peek()).getPriority() >= token.getPriority()))
            {
                output.append(" ");
                output.append(opstack->tryPop());
            }
            
            string buff="";//= "";
            buff.push_back(character);
            opstack->push(buff);
            output.append(" ");
            
        }
        
    }
    while (!opstack->isEmpty())
    {   
        output.append(" ");
        output.append(opstack->tryPop());
        
    }
    return output;
}
