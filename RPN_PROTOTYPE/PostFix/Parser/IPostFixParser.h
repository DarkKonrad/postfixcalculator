#pragma once
#include <string>

class IPostFixParser
{
public:
	virtual std::string ConvertToPostFix(std::string statement) = 0;
	virtual ~IPostFixParser() {}
};