#pragma once
#include "IPostFixParser.h"
#include "../../Common/Utils/Utils.h"
#include "../../Common/Stack/iStack.h"
#include "../../Common/Stack/NativeStack.h"
#include <vector>

class PostFixParser : public IPostFixParser
{
protected:
	iStack<std::string>* opstack;
public:
	std::string ConvertToPostFix(std::string statement) override;
	PostFixParser() : opstack(new NativeStack<std::string>()){}
	PostFixParser(iStack<std::string>* stack) : opstack(stack){}
	~PostFixParser() { delete opstack; }
};

