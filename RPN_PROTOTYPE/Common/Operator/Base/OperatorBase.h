#pragma once
#include "../Properties/OperatorProperties.h"
#include <string>
#include <map>
namespace RpnOperator
{

	class OperatorBase 
	{
	//private:

	//	std::string symbol;
	//	unsigned short priority;
	//	Associativity associativity;

	//public:
	//	OperatorBase(std::string Symbol, Associativity Associativity) : symbol{ Symbol }, priority{ getPriority(Symbol) }, associativity{Associativity} {}
	//	OperatorBase() : symbol{ "" }, priority{ 0 }, associativity{ Associativity::Left} {}
	//	unsigned short getPriority(std::string symbol) { return priorityMap.at(symbol); }

	//	//getters
	//	const std::string getSymbol() const { return this->symbol;}
	//	const unsigned short getPriority() const { return this->priority;}
	//	const Associativity getAssociativity() const { return this->associativity; }
		
	protected: 
		std::string symbol;
		OperatorProperties operatorProperties;
	
	public:			

		OperatorBase() : symbol{ "" } {}
		OperatorBase(std::string Symbol);
		OperatorBase(std::string Symbol, OperatorProperties operProp);
		
		
		OperatorProperties getOperatorProperties() const;
		// Pure Virtual
		virtual long double Evaluate(long double left, long double right)=0;
		virtual std::string getSymbol();
	};


}
