#include "OperatorBase.h"
#include <string>
#include <stdexcept>

using namespace RpnOperator;
using std::string;


OperatorBase::OperatorBase(string Symbol, OperatorProperties operProp) : symbol{ Symbol }, operatorProperties{ operProp } {}

OperatorBase::OperatorBase(string Symbol) : symbol(Symbol)
{
	try 
	{
		this->operatorProperties = OperatorProperties::FindOperatorProperties(Symbol);
	
	}
	catch (std::out_of_range)
	{
		this->operatorProperties = OperatorProperties();
	}
}


OperatorProperties OperatorBase::getOperatorProperties() const
{
	return this->operatorProperties;
}

std::string RpnOperator::OperatorBase::getSymbol()
{
	return this->symbol;
}

	
	
