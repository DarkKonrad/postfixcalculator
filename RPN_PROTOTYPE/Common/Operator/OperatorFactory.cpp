#include "OperatorFactory.h"

using namespace RpnOperator;

    OperatorBase* RpnOperator::OperatorFactory::getOperator(char Symbol)
    {
        switch (Symbol)
        {
            
            case '+': return new AddOperator();
                break;
            case '-': return new SubOperator();
                break;
            case '*': return new MulOperator();
                break;
            case '/': return new DivOperator();
                break;
            case '^': return new PowOperator();
                break;
            case '%': return new ModOperator();
                break;
            default: throw symbolNotFoundException();
                break;
        }
        return nullptr;
    }

    OperatorBase* RpnOperator::OperatorFactory::getOperator(std::string Symbol)
    {
        if (Symbol.size() > 1)
            throw notAnOperatorException();

        auto oper = Symbol[0];

        return getOperator(oper);
    }




