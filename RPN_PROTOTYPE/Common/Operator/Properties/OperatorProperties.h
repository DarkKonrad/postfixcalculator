#pragma once
#include <map>
#include <string>

namespace RpnOperator
{
	enum class Associativity
	{
		Left,
		Right,
		None
	};

	class OperatorProperties
	{

	private:	
			unsigned short priority;
			Associativity associativity;
			const static  std::map<std::string, OperatorProperties> properitesMap;
	public:	
			explicit OperatorProperties(unsigned short Priority, Associativity asc) : priority{ Priority }, associativity{ asc } {}
			OperatorProperties() : priority{ 0 }, associativity{ Associativity::None } {}
			OperatorProperties(const OperatorProperties& other) { priority = other.priority; associativity = other.associativity; }

			static OperatorProperties FindOperatorProperties(std::string Symbol);
			static OperatorProperties FindOperatorProperties(char Symbol);
			unsigned short getPriority() const;
			Associativity getAssiciativity() const;
	};


	
}

