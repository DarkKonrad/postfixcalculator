#include "OperatorProperties.h"
#include <string>

using namespace RpnOperator;


	OperatorProperties OperatorProperties::FindOperatorProperties(std::string Symbol)
	{
		auto result = properitesMap.at(Symbol);
		return result;
	}
	
	OperatorProperties OperatorProperties::FindOperatorProperties(char Symbol)
	{
		std::string buff = "";
		buff.push_back(Symbol);

		auto result = properitesMap.at(buff);
		return result;
	}

	unsigned short OperatorProperties::getPriority() const
	{
		return this->priority;
	}

	Associativity OperatorProperties::getAssiciativity() const
	{
		return this->associativity;
	}

	const std::map<std::string, RpnOperator::OperatorProperties> OperatorProperties::properitesMap
	{
		{std::string("(") , OperatorProperties{1,RpnOperator::Associativity::Left} },
		{std::string("-") , OperatorProperties{2,RpnOperator::Associativity::Left} },
		{std::string("+") , OperatorProperties{2,RpnOperator::Associativity::Left} },
		{std::string("*") , OperatorProperties{3,RpnOperator::Associativity::Left} },
		{std::string("/") , OperatorProperties{3,RpnOperator::Associativity::Left} },
		{std::string("%") , OperatorProperties{3,RpnOperator::Associativity::Left} },
		{std::string("^") , OperatorProperties{4,RpnOperator::Associativity::Right} }
	};

