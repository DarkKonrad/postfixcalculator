#pragma once
#include "Base/OperatorBase.h"
#include "Properties/OperatorProperties.h"
#include "Concrete/AddOperator.h"
#include "Concrete/DivOperator.h"
#include "Concrete/ModOperator.h"
#include "Concrete/MulOperator.h"
#include "Concrete/PowOperator.h"
#include "Concrete/SubOperator.h"

#include <string>
#include <exception>
namespace RpnOperator
{
	class OperatorFactory
	{
	public:
		OperatorBase* getOperator(char Symbol);
		OperatorBase* getOperator(std::string Symbol);
	};
}

class notAnOperatorException : public std::exception
{
public:
	virtual const char* what() const throw() override
	{
		return "Not an operator symbol. Expected one-element string with operator symbol";
	}
};

class symbolNotFoundException : public std::exception
{
public:
	virtual const char* what() const throw() override
	{
		return "Operator/Symbol not found or not defined";
	}
};