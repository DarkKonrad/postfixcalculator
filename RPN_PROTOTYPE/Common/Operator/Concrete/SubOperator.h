#pragma once
#include "../Base/OperatorBase.h"
#include <string>

namespace RpnOperator
{
	class SubOperator : public OperatorBase
	{
	public:
		SubOperator() : OperatorBase("-") {}

		long double Evaluate(long double left, long double right) override
		{
			return (long double)(left - right);
		}
	};
}
