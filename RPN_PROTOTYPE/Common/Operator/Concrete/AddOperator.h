#pragma once
#include "../Base/OperatorBase.h"
#include <string>
namespace RpnOperator
{
	class AddOperator : public OperatorBase
	{
	public:
		AddOperator() : OperatorBase("+") {}

		long double Evaluate(long double left, long double right) override
		{
			return (long double)(left + right);
		}
	};
}
