#pragma once
#include "../Base/OperatorBase.h"
#include <string>
#include <cmath>
namespace RpnOperator
{
	class ModOperator : public OperatorBase
	{
	public:
		ModOperator() : OperatorBase("%") {}

		long double Evaluate(long double left, long double right) override
		{	
			return std::fmodl(left, right);
		}
	};
}
