#pragma once
#include "../Base/OperatorBase.h"
#include <string>
#include <cmath>
namespace RpnOperator
{
	class PowOperator : public OperatorBase
	{
	public:
		PowOperator() : OperatorBase("^") {}

		long double Evaluate(long double left, long double right) override
		{
			return std::powl(left, right);
		}
	};
}
