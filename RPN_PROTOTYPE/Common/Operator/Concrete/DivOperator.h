#pragma once
#include "../Base/OperatorBase.h"
#include <string>
namespace RpnOperator
{
	class DivOperator : public OperatorBase
	{
	public:
		DivOperator() : OperatorBase("/") {}

		long double Evaluate(long double left, long double right) override
		{
			return (long double)(left / right);
		}
	};
}
#pragma once
