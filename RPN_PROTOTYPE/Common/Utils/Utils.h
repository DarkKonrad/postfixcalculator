#pragma once
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "../Operator/Properties/OperatorProperties.h"
#include <regex>
#include <vector>

namespace Utils
{
    std::vector<std::string> split(const std::string& input, const std::string& regex);

    bool isOperator(std::string character);

    bool isOperator(char character);
   
}
