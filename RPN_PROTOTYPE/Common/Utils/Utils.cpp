#include "Utils.h"

using namespace Utils;


bool Utils::isOperator(std::string character)
{
    if (character.size() != 1)
        return false;
    try
    {
        auto token = RpnOperator::OperatorProperties::FindOperatorProperties(character);
        return true;
    }
    catch (std::out_of_range)
    {
        return false;
    }

    return false;
}

std::vector<std::string> Utils::split(const std::string& input, const std::string& regex)
{
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    std::sregex_token_iterator
        first{ input.begin(), input.end(), re, -1 },
        last;
    return { first, last };
}

bool Utils::isOperator(char character)
{
    std::string buff;
    buff.push_back(character);

    return isOperator(buff);
}

