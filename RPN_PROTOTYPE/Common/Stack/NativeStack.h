#pragma once
#include "iStack.h"
#include <stack>

template <class Type>
class NativeStack : public iStack<Type>
{
	std::stack<Type> stack;

	unsigned int size()override;

	void clear() override;

	void push(Type item) override;
	bool isEmpty() override;

	Type pop() override;
	Type peek() override;
};

template<class Type>
inline unsigned int NativeStack<Type>::size()
{
	return stack.size();
}

template<class Type>
inline void NativeStack<Type>::clear()
{
	stack = std::stack<Type>();
}

template<class Type>
inline void NativeStack<Type>::push(Type item)
{
	stack.push(item);
}

template<class Type>
inline bool NativeStack<Type>::isEmpty()
{
	return stack.empty();
}

template<class Type>
inline Type NativeStack<Type>::pop()
{
	auto buffor = stack.top();
	stack.pop();

	return buffor;
}

template<class Type>
inline Type NativeStack<Type>::peek()
{
	return stack.top();
}
