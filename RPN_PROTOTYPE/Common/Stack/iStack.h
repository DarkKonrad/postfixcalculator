#pragma once
#include "IExceptTryPop.h"

/// Interface Stack 
template <class Type>
class iStack : public ExceptTryPop <Type>
{
	
	//Methods
public:
	virtual	unsigned int size()=0;

	virtual	void clear()=0;

	virtual void push(Type item)=0;
	virtual bool isEmpty()=0;

	virtual Type pop()=0;
	virtual Type peek()=0;

	virtual Type tryPop() override
	{
		if (isEmpty() == true)
			throw emptyStackException();
		
		return pop();
	}
};