#pragma once
#include <exception>

template <class popType>
class ExceptTryPop
{
public:
	virtual popType tryPop() = 0;
};

class emptyStackException : std::exception
{
public:
	virtual const char* what() const throw() override
	{
		return "Pop called on empty stack";
	}
};