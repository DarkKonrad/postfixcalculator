#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_PROTOTYPE_RpnGUI.h"
#include "qpushbutton.h"

class PROTOTYPE_RpnGUI : public QMainWindow
{
    Q_OBJECT

public:
    PROTOTYPE_RpnGUI(QWidget *parent = Q_NULLPTR);

private:
    Ui::PROTOTYPE_RpnGUIClass ui;
    bool lockComma;
    void init();
private slots:
    void basicButtonClicked();
    void unlockComma_basicButtonClicked();
    void clearAll();
    void clearLastChar();
    void getResult();
};
