#include "PROTOTYPE_RpnGUI.h"
#include "stdafx.h"
#include "Parser/PostFixParser.h"
#include "Evaluator/PostFixEvaluator.h"
#include <exception>
PROTOTYPE_RpnGUI::PROTOTYPE_RpnGUI(QWidget* parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	init();
	lockComma = false;
}

void PROTOTYPE_RpnGUI::basicButtonClicked()
{
	auto display = this->ui.lEdt_InFix;

	QPushButton* clickedButton = qobject_cast<QPushButton*>(sender());
	QString clickedValue = clickedButton->text();

	if (clickedValue == "mod")
		clickedValue = "%";

	if (clickedValue == "," || clickedValue == ',')
	{
		if (!lockComma)
			lockComma = true;
		else
			return;
	}

	display->setText(display->text() + clickedValue);
}
void PROTOTYPE_RpnGUI::clearAll()
{
	auto display = this->ui.lEdt_InFix;
	auto postFixDisplay = this->ui.lEdt_PostFix;
	auto logDisplay = this->ui.txtEdt_OperationLog;

	this->lockComma = false;
	display->setText("");
	postFixDisplay->setText("");
	logDisplay->setText("");
}

void PROTOTYPE_RpnGUI::clearLastChar()
{
	auto display = this->ui.lEdt_InFix;
	auto postFixDisplay = this->ui.lEdt_PostFix;
	auto logDisplay = this->ui.txtEdt_OperationLog;

	QString displayText = display->text();
	unsigned int size = displayText.size();
	
	if (displayText[displayText.size() - 1] == ',')
		lockComma = false;

	displayText.resize(size - 1);
	display->setText(displayText);
	postFixDisplay->setText("");
	logDisplay->setText("");
}

void PROTOTYPE_RpnGUI::unlockComma_basicButtonClicked()
{
	lockComma = false;
	basicButtonClicked();
}

void PROTOTYPE_RpnGUI::getResult()
{
	auto display = this->ui.lEdt_InFix;
	auto logDisplay = this->ui.txtEdt_OperationLog;
	auto postFixDisplay = this->ui.lEdt_PostFix;

	QString rawInput = display->text();
    rawInput = rawInput.replace(',', ".");
	auto rawInput_std = rawInput.toStdString();
	
	PostFixParser parser;
	try
	{
		QString postFixStatement = QString::fromStdString(
		parser.ConvertToPostFix(rawInput_std));

		postFixDisplay->setText(postFixStatement);
	
		PostFixEvaluator evaluator(rawInput_std);

		QString calculationResult = QString::number(evaluator.Evaluate(), 'f');
		calculationResult = calculationResult.replace('.', ",");

		QString logs = QString::fromStdString(evaluator.getLogs());

		display->setText(calculationResult);
		logDisplay->setText(logs);
	}
	catch(emptyStackException& ex)
	{
		display->setText("Error");
		logDisplay->setText(ex.what());
		postFixDisplay->setText("Error");
	}
	catch (std::exception& ex)
	{
		display->setText("Error");
		logDisplay->setText(ex.what());
		postFixDisplay->setText("Error");
	}
}

void PROTOTYPE_RpnGUI::init()
{
	//Digit buttons
	connect(ui.button_0, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_1, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_2, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_3, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_4, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_5, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_6, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_7, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_8, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_9, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));

	//Operator Buttons
	connect(ui.button_add, SIGNAL(clicked()), this, SLOT(unlockComma_basicButtonClicked()));
	connect(ui.button_div, SIGNAL(clicked()), this, SLOT(unlockComma_basicButtonClicked()));
	connect(ui.button_sub, SIGNAL(clicked()), this, SLOT(unlockComma_basicButtonClicked()));
	connect(ui.button_mod, SIGNAL(clicked()), this, SLOT(unlockComma_basicButtonClicked()));
	connect(ui.button_mul, SIGNAL(clicked()), this, SLOT(unlockComma_basicButtonClicked()));
	connect(ui.button_pow, SIGNAL(clicked()), this, SLOT(unlockComma_basicButtonClicked()));
	connect(ui.button_openBrac, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_closeBarc, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_comma, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));

	//Other
	connect(ui.button_comma, SIGNAL(clicked()), this, SLOT(basicButtonClicked()));
	connect(ui.button_clearAll, SIGNAL(clicked()), this, SLOT(clearAll()));
	connect(ui.button_clearChar, SIGNAL(clicked()), this, SLOT(clearLastChar()));
	connect(ui.button_result, SIGNAL(clicked()), this, SLOT(getResult()));
}
