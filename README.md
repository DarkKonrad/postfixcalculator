**PostFix / RPN Caluclator project** 

Project was made for university class and contain free subprojects:
- Parser 
    - Converts InFix statement to PostFix
- Evaluator
    - Handles mathematic operations on PostFix statement
- Gui 
    - Provides user-interface with additional insight to particular steps of formulating final cacluation.

Gui is made with usage of QT Framework so in order to compile project QT 5.9.9 instalation with QT for Visual Studio addon is required.
